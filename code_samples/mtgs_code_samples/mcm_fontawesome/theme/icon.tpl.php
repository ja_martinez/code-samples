<?php
/**
 * @file
 * Icon tpl.
 *
 * $type: The type of icon being rendered.
 * $label: A text-based label to display with the icon.
 * $path: A URL to link this icon and label to.
 * $attributes: A set of attributes to include with the icon.
 * $attributes_array: The icon attributes in an array format.
 * $link_attributes: A set of attributes to include with the link.
 * $link_attributes_array: The link attributes in an array format.
 */
?>
<?php if (!empty($url)) : ?>
<a href="<?= $url ?>"<?= $link_attributes ?>><?= $label ?><i class="<?= $classes ?>"<?= $attributes ?>></i></a>
<?php else : ?>
<?= $label ?><i class="<?= $classes ?>"<?= $attributes ?>></i>
<?php endif; ?>
