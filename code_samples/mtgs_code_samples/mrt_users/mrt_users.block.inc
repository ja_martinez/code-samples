<?php
/**
 * @file
 * User Blocks hooks.
 */

/**
 * Implements hook_block_info().
 */
function mrt_users_block_info() {
  $blocks = array();

  $blocks['mrt_users_user_block'] = array(
    'info' => t('User Details'),
    'cache' => DRUPAL_CACHE_PER_USER | DRUPAL_CACHE_PER_PAGE,
    'status' => TRUE,
    'visibility' => BLOCK_VISIBILITY_LISTED,

  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function mrt_users_block_view($delta = '') {
  switch ($delta) {

    case 'mrt_users_user_block':
      return mrt_users_user_block_view();
  }
}

/**
 * Implements hook_block_view().
 */
function mrt_users_user_block_view() {

  $block = array(
    'subject' => t('My Sets'),
    'content' => array(),
    'edit_link' => array(
      '#type' => 'container',
      '#attributes' => array('class' => array('mrt-link-user-edit-wrapper')),
      '#access' => FALSE,
      'link-0' =>  array(
        '#theme'  => 'link',
        '#text'    => t('Edit profile'),
        '#path'  => NULL,
        '#options' => array(
          'attributes' => array('class' => array('mrt-link-user-edit')),
          'html' => FALSE,
        ),
      ),
    ),
  );

  $current_user = menu_get_object('user', 1);
  $current_node = menu_get_object();

  // We need to either be looking at a user or viewing a node. If neither of
  // these are true then this block doesn't make sense.
  if (empty($current_user) && empty($current_node)) {

    return $block;
  }

  // Further restrict that if we are looking at a node it needs to NOT have any
  // members in order to see a user profile block.
  if (empty($current_user) && !empty($current_node)) {

    if (module_exists('mrt_collaboration') && mrt_collaboration_get_set_members($current_node, TRUE)) {

      return $block;
    }
  }

  // Determine which user we should use: Viewed or node.
  $block_user = empty($current_user) ?
    user_load($current_node->uid) :
    $current_user;

  $block['content'][] = user_view($block_user, 'user_block');

  // Allow users to edit their own profile or anyone with the admin permission.
  $can_edit_profile = $block_user->uid == $GLOBALS['user']->uid || user_access('administer users');

  $block['edit_link']['#access'] = $can_edit_profile;
  $block['edit_link']['link-0']['#path'] = 'user/' . $block_user->uid . '/edit';

  return $block;
}

/**
 * Implements hook_preprocess_block().
 */
function mrt_users_preprocess_block(&$variables) {

  $variables['theme_hook_suggestions'][] = 'block__' . $variables['block']->delta;
}

/**
 *  Implements hook_block_list_alter
 */
function mrt_users_block_list_alter(&$blocks){

  $node = menu_get_object();

  // Only display the user profile block on certain pages.
  $is_user_page = arg(0) === 'user' && is_numeric(arg(1));
  $is_user_subpage = arg(0) === 'user' && !is_null(arg(2));
  $is_set_page = !empty($node) && 'sets' === $node->type && is_null(arg(2));
  $is_shared_set = $is_set_page && module_exists('mrt_collaboration') && count(mrt_collaboration_get_set_members($node, TRUE)) > 0;

  $hide_user_block = !(($is_user_page && !$is_user_subpage) || ($is_set_page && !$is_shared_set));

  $blocks_to_hide = array(
    array(
      'module' => 'mrt_users',
      'delta' => 'mrt_users_user_block',
      'hide' => $hide_user_block,
    ),
    array(
      'module' => 'delta_blocks',
      'delta' => 'page-title',
      'hide' => (arg(0) == 'user' || arg(0) == 'profile') && !user_is_anonymous(),
    ),
    array(
      'module' => 'search_api_page',
      'delta' => 'properties',
      'hide' => !empty($node) && 'property' === $node->type,
    ),
  );

  foreach ($blocks as $bid => $block) {

    foreach ($blocks_to_hide as $info) {

      $is_same_block = $info['delta'] === $block->delta && $info['module'] === $block->module;

      if (!empty($info['hide']) && $is_same_block) {

        unset($blocks[$bid]);
      }
    }
  }
}
