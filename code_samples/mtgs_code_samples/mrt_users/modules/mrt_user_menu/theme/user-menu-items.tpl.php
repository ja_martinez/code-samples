<?php
/**
 * display a list of items associated with the user
 */
?>
<p />
<?php if(!empty($items)): ?>

    <?php foreach(element_children($items) as $delta) : ?>
    <?php $item = $items[$delta]; ?>


      <?php print render($item); ?>


    <?php endforeach; ?>

<?php endif; ?>
