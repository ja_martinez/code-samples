<h2 class="title">Register</h2>
<h3>Every Meeting Has a Purpose. What's Yours?</h3>
<div class="mrt-user-form-wrapper">
  <div class="mrt-user-main">
    <?php
      print render($form['field_first_name']);
      print render($form['field_last_name']);
      print render($form['account']);
      print render($form['field_phone']);
      print render($form['field_company']);
      print render($form['field_website']);
      print render($form['field_user_address']);
      print render($form['field_about_me']);
    ?>
  </div>
  <div class="mrt-user-picture">
    <div class="mrt-user-picture-wrapper">
    <?php print render($form['picture']) ?>
  </div>
  </div>
  <div class="mrt-user-bottom">
    <div class="description">
      <span class="mrt-primary">*</span> Required
    </div>
    <?php print drupal_render_children($form) ?>
  </div>
</div>
