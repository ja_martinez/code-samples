<?php
/**
 * @file
 * Callback implementations.
 */

/**
 * Page callback for user uploaded files.
 *
 * @param object $account
 *   The Drupal user being viewed.
 *
 * @return array
 *   Drupal render array.
 */
function mrt_user_files_uploaded_files_page($account) {

  $build = array();

  $build['content']['upload'] = drupal_get_form('mrt_user_files_multiupload_form');

  $build['content']['upload']['#suffix'] = '<hr>';

  $build['content']['files'] = array(
    '#theme'  => 'html_tag',
    '#tag'    => 'span',
    '#value'  => t('There are no uploads.'),
    '#attributes'  => array('class' => 'mrt-user-files-no-uploads')
  );


  $query = db_select('file_managed', 'm')
            ->distinct()
            ->fields('m', array('fid'))
            ->condition('m.type', array('image', 'video'))
            ->condition('m.uid', $account->uid)
            ->orderBy('m.fid', 'DESC')
            ->extend('PagerDefault')
            ->limit(60);

  $results = $query->execute()->fetchCol();

  if (!empty($results)) {

    $files = file_load_multiple($results);
    $build['content']['files'] = drupal_get_form('mrt_user_files_edit_uploads_list_form', $files);

    $build['content']['pager'] = array(
      '#theme' => 'pager',
      '#weight' => 5,
    );
  }

  return $build;
}

/**
 * Access callback for mrt_user_files_uploaded_files_page().
 */
function mrt_user_files_uploaded_files_access($account) {

  $can_access = user_access('administer files');
  $can_access |= $account->uid == $GLOBALS['user']->uid && user_access('view own files');

  return $can_access;
}

/**
 * Page callback (form) for when a user wants to hide a published file.
 *
 * Published in this case means it is already in the Inspiration Gallery. The
 * users should be able to hide them from their "Uploads" list.
 */
function mrt_user_files_hide_file($form, &$form_state, $file) {
  $form_state['file'] = $file;

  $form['fid'] = array(
    '#type' => 'value',
    '#value' => $file->fid,
  );

  $description = t('This action will remove the file from your Personal Photo Gallery. The file will remain visible in the Inspiration Gallery.');

  return confirm_form($form,
    t('Are you sure you want to remove the file %title from your Personal Photo Gallery?', array(
      '%title' => entity_label('file', $file),
    )),
    'file/' . $file->fid,
    $description,
    t('Remove File')
  );
}

/**
 * Form submission handler for file_entity_delete_form().
 */
function mrt_user_files_hide_file_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] && $file = file_load($form_state['values']['fid'])) {

    $target_user = user_load_by_name('Marriott');

    if (!empty($target_user)) {
      $file->uid = $target_user->uid;
      file_save($file);
      drupal_set_message(t('The file has been removed from your Personal Photo Gallery.'));
    }
    else {
      drupal_set_message(t('The file was not able to be removed from your Personal Photo Gallery at this time. We apologize for the inconvenience.'), 'error');
      watchdog('mrt_user_files', 'The generic Marriott user account was not available to transfer file ownership to.');
    }
  }

  $form_state['redirect'] = 'user/uploads';
}

/**
 * Access callback to see whether a user should be able to hide a file.
 */
function mrt_user_files_user_can_hide_file($file) {

  $account = $GLOBALS['user'];

  // Users can only hide their own files.
  if ($account->uid != $file->uid) {
    return FALSE;
  }

  // Users can only hide published (in-gallery) files.
  if (FILE_PUBLISHED != $file->published) {
    return FALSE;
  }

  try {

    $wrapper = entity_metadata_wrapper('file', $file);
    // Only Gallery type photos can be removed.
    if ($wrapper->field_photo_type->raw() != MRT_USER_FILES_PHOTO_TYPE_GALLERY) {

      return FALSE;
    }
  }
  catch (Exception $x) {

    // Not a notable exception.
  }

  return TRUE;
}
