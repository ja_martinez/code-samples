(function ($) {

  Drupal.behaviors.mrt_user_files_plupload = {

    attach: function (context, settings) {

      // This is needed to tell Plupload which of our edit buttons to auto-click.
      if (typeof Drupal.settings.plupload['edit-widget'] !== 'undefined') {
        $('#edit-submit').click(function() {
          Drupal.settings.plupload['edit-widget'].submit_element = '#edit-submit';
        });

        $('#edit-submit-gallery').click(function() {
          Drupal.settings.plupload['edit-widget'].submit_element = '#edit-submit-gallery';
        });
      }
    }
  };
})(jQuery);
