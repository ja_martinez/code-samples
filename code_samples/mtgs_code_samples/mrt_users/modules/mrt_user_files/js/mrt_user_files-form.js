(function ($) {

  Drupal.behaviors.mrt_user_files_image_type = {

    attach: function (context, settings) {

      // Show/hide fieldsets based on the photo type.
      // We can't use the states API because the fieldset doesn't exist at
      // form build time.
      $('input[name^="field_photo_type"]', context)
        .once()
        .on('change', function() {

          // Change event can fire for any radio, not just the selected. Make
          // sure we only proceed when we're working with the selected item.
          if (!$(this).is(':checked')) {

            return;
          }

          var elements = $('.fieldset-usage-rights, .fieldset-details');
          switch ($(this).val()) {

            // Personal.
            case '1':
              elements.stop(true, true).slideUp();
              break;

            // Gallery.
            case '2':
              elements.stop(true, true).slideDown();
              break;

            default:
              elements.stop(true, true).show();
              break;
          }
        })
        .trigger('change');
    }
  };
})(jQuery);
