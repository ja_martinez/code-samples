# User File Uploads

This module handles functionality related to users uploading files. The bulk of
its code deals with allowing users to upload multiple files at once and edit
file details.
