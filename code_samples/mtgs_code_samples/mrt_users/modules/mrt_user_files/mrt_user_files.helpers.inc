<?php
/**
 * @file
 * Helper functions for working with user uploads.
 */

/**
 * Check if the current user has edit access to the given file.
 *
 * @param entity $file
 *   The file entity to check access on.
 *
 * @return bool
 *   TRUE if they have access, FALSE otherwise.
 */
function mrt_user_files_user_has_access_to_file($file) {

  if (empty($file->uid)) {

    return FALSE;
  }

  if (file_entity_access('update', $file)) {

    return TRUE;
  }

  watchdog('mrt_user_files',
          'User @uid did not have access to edit @uuid.',
          array('@uuid' => $uuid, '@uid' => $GLOBALS['user']->uid),
          WATCHDOG_NOTICE);

  return FALSE;
}
