(function($) {
  Drupal.behaviors.mrt_users_profile_form = {
    attach: function(context, settings) {

      $('#user-profile-form, #user-register-form', context).once('users-profile-edit-processed', function() {

        $('#edit-picture-upload').hide();
        $('#edit-picture-upload').change(function (){
          // Showing input field based on if empty or not
          if($('#edit-picture-upload').val() == '') {
            $('#edit-picture-upload').hide();
          }
          else {
            $('#edit-picture-upload').show();
          }

        });

      });
    }
  };
})(jQuery);