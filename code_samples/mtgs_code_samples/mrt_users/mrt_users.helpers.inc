<?php
/**
 * @file
 * Users related helper functions.
 */

 /**
  * function to clear user details block cache
  */
  function _mrt_users_clear_cache_user_details_block() {
    
    // Memcache won't be cleared unless this is a wildcard call
    cache_clear_all('*', 'cache_block', TRUE);
    
  }
