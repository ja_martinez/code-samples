<div class="mrt-user-form-wrapper">
  <div class="mrt-user-main">
    <?php
      hide($form['picture']);
      print drupal_render_children($form);
    ?>
  </div>
  <div class="mrt-user-picture">
    <div class="mrt-user-picture-wrapper">
    <?php print render($form['picture']) ?>
  </div>
  </div>
</div>
