<?php
//user block tpl

?>

<div id="block-<?php print $block->module . '-' . $block->delta; ?>" class="<?php print $classes; ?> " <?php print $attributes; ?>>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div class="mrt-users-pic-wrapper">
      <?php print render($elements[0]['user_picture']); ?>
    </div>
    <div class="mrt-users-data-wrapper">
      <?php print render($title_prefix); ?>
      <?php if ($block->subject): ?>
        <span class="mrt-users-block-title"><?php print $block->subject ?></span>
      <?php endif;?>
      <div class="mrt-users-name">
        <?php
          print render($elements[0]['field_first_name']);
          print render($elements[0]['field_last_name']);
          print render($block->edit_link);
        ?>
      </div>
      <div class="mrt-users-job-web-icon">
        <div class="mrt-users-loc-icon">
          <?php print render($elements[0]['location_icon']); ?>
        </div>
        <div class="mrt-users-job-web">
          <?php
            print render($elements[0]['field_website']);
          ?>
        </div>
      </div>
      <div class="mrt-users-company-loc">
        <?php
          print render($elements[0]['field_company']);
          print render($elements[0]['field_user_address']);
        ?>
      </div>
      <div class="mrt-users-phone-email">
        <?php
          print render($elements[0]['field_phone']);
          print render($elements[0]['user_email']);
        ?>
      </div>
      <div class="mrt-users-about">
      <?php
        print render($elements[0]['field_about_me']);
      ?>
      </div>
    </div>
  </div>
  <div class="mrt-users-chevron-d-icon">
    <?php print render($elements[0]['cheveron_down_icon']); ?>
  </div>
</div>

