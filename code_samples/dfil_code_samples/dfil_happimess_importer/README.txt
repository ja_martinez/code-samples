INSTRUCTIONS ON HOW TO USE HOOK_QUEUE_HAPPIMESS:

Create a module that's purpose is to iterate over a data set that's returned
from any social network source (Twitter, FB, etc). Create a function like so:

function dfil_happimess_twitter_test_queue_happimess($hashtag = NULL) {

  // get your list of posts
  $data = dfil_happimess_twitter_fetch_data($hashtag);

  // iterate over the list, turning it into a node-friendly object
  foreach ($data as $item) {
    $result = new stdClass();

    $result->body_text = $item->body;
    $result->title = $item->title;

    $results[] = $result;
  }

  return $results;
}

Look in dfil_happimess_importer_process() to see how nodes are saved to the DB.