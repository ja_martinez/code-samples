/**
 * @file
 * Loads and initializes the Facebook JavaScript SDK.
 */

(function ($) {

  /**
   * Trigger an event so other code can react to when FB is available.
   */
  function fb_api_is_ready() {

    $('body').addClass('fb-js-sdk');
    $(document).trigger('facebook:ready');
  }

  /**
   * Include the FB JS SDK.
   */
  function fb_async_include() {

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  // See if FB is already defined from another import.
  if (window.FB) {

    fb_api_is_ready();
  }
  else {

    // Setup an init handler so our FB object gets created.
    window.fbAsyncInit = function () {

        FB.init({
          appId      : Drupal.settings.Happimess_Facebook.appid,
          // Performance perk since we're not using the sharing via SDK.
          xfbml      : false,
          version    : 'v2.0'
        });

        // API is ready for use at this point.
        fb_api_is_ready();
    };
  }

  // Include FB JS SDK.
  fb_async_include();
})(jQuery);
