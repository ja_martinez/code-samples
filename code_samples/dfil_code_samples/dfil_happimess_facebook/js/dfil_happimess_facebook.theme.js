/**
 * @file
 * Theme related functions for dealing with Facebook images and albums.
 */

(function ($, Drupal) {
  "use strict";

  /**
   * Renders a Facebook album name.
   *
   * @param string
   *   The name of the album.
   *
   * @return string
   *   The rendered markup.
   */
  Drupal.theme.prototype.facebookAlbumName = function (name) {

    var markup = $('<div></div>');
    markup.addClass('fb-album-name');
    markup.text(name);

    return markup.prop('outerHTML');
  };

  /**
   * Renders a Facebook album image.
   *
   * @param object photo
   *   The image to render.
   *
   * @return string
   *   The rendered image.
   */
  Drupal.theme.prototype.facebookAlbumImage = function (photo) {

    var image = $('<img></img>').attr('src', photo.picture);
    image.attr('id', photo.id);
    image.attr('title', photo.name);

    var markup = $('<div></div>');
    markup.addClass('fb-album-image');
    markup.append(image);

    return markup.prop('outerHTML');
  };

  /**
   * Renders a Facebook album.
   *
   * @param FacebookAlbum album
   *   The album to render.
   *
   * @return string
   *   The rendered album.
   */
  Drupal.theme.prototype.facebookAlbum = function (album) {

    var cover_photo = album.getCoverPhoto();

    var markup = $('<div></div>');
    markup.addClass('fb-album');
    markup.attr('id', album.info.id);
    markup.attr('title', album.info.name);
    markup.append(Drupal.theme('facebookAlbumImage', cover_photo));
    markup.append(Drupal.theme('facebookAlbumName', album.info.name));

    return markup.prop('outerHTML');
  };

  /**
   * Renders a list of Facebook albums.
   *
   * @param FacebookAlbumCollection albums
   *   A collection of Facebook albums.
   *
   * @return string
   *   The rendered list of albums.
   */
  Drupal.theme.prototype.facebookAlbums = function (albums) {

    var markup = $('<ul></ul>');
    markup.addClass('fb-album-inline');
    markup.addClass('fb-albums');

    // We build a string full of all <li></li> because it is faster than using
    // multiple calls to append.
    var list_items = '';
    $.each(albums, function (idx, album) {

      list_items += '<li>' + album.render() + '</li>';
    });

    markup.append(list_items);
    return markup.prop('outerHTML');
  };

  /**
   * Renders a Facebook album's images.
   *
   * @param FacebookAlbum album
   *   The album to render images for.
   *
   * @return string
   *   The rendered list of images.
   */
  Drupal.theme.prototype.facebookAlbumImages = function (album) {

    var markup = $('<ul></ul>');
    markup.attr('id', album.info.id);
    markup.attr('title', album.info.name);
    markup.addClass('fb-album-inline');
    markup.addClass('fb-album-images');

    // We build a string full of all <li></li> because it is faster than using
    // multiple calls to append.
    var list_items = '';
    $.each(album.info.photos.data, function (idx, photo) {

      list_items += '<li>' + Drupal.theme('facebookAlbumImage', photo) + '</li>';

    });
    markup.append(list_items);

    return markup.prop('outerHTML');
  };

  /**
   * Builds a preview for a selected Facebook image.
   *
   * @param object photo
   *   The selected photo.
   *
   * @param object album
   *   The album the selected photo belongs to.
   *
   * @return string
   *   The preview markup.
   */
  Drupal.theme.prototype.facebookSelectedPhotoPreview = function (photo, album) {

    var markup = $('<div></div>');
    markup.addClass('fb-preview');

    var image = $('<img></img>');
    image.attr('src', photo.picture);
    image.addClass('fb-preview-image');

    var text = $('<p></p>');
    text.addClass('fb-preview-text');
    text.text(
      Drupal.t('You have selected this image from the @album album. Press cancel to make a new selection.', {
        '@album': album.info.name
      }));

    markup.append(image);
    markup.append(text);

    return markup.prop('outerHTML');
  };
})(jQuery, Drupal);
