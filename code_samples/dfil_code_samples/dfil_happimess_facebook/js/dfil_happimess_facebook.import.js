(function ($, Drupal) {
  "use strict";

  // Global FB album array to keep track of loaded albums.
  var facebook_albums = new FacebookAlbumCollection();

  /**
   * Creates a hidden jQuery UI dialog with some sane defaults.
   * @return object
   *   A jQuery element.
   */
  function create_browse_dialog() {

    var dialog = $('<div></div>');
    dialog
      .attr('title', Drupal.t('Select a photo.'))
      .attr('id', 'fb-album-dialog')
      .dialog({
        autoOpen: false,
        position: 'center',
        width: $(window).width() * .8,
        maxHeight: $(window).height() * .8,
        modal: true,
        draggable: false,
        resizable: false,
        close: function (event, ui) { $(this).dialog('destroy'); }
      });

    return dialog;
  };

  /**
   * Converts the FB API response into FacebookAlbum objects.
   *
   * @param array album_items
   *   An array of items returned by the Facebook Albums API request.
   */
  function process_fb_albums(album_items) {

    // Clear out any existing items.
    facebook_albums.clear();
    $.each(album_items, function (idx, item) {

      // Only add albums that have photos available.
      if (item.photos.data.length > 0) {

        facebook_albums.push(new FacebookAlbum(item));
      }
    });
  }

  /**
   * Resize the given jQuery UI dialog
   * @param object dialog
   *   The jQuery UI dialog element.
   */
  function resize_browse_dialog(dialog) {

    // Get the dialog's width so we can optimize the number of items per row.
    var dialog_width = parseInt(dialog.dialog('option', 'width'));

    // How wide is one li including margins and padding.
    var item_width = Math.ceil($('li', dialog).outerWidth(true));

    // Account for inner container padding.
    var adjustment = parseInt(dialog.css('padding-left')) + parseInt(dialog.css('padding-right'));

    // Multiple item_width by the number of items per row desired.
    var new_size = adjustment + Math.floor(dialog_width / item_width) * item_width;

    // Determine if we have too few items to fill out our suggested size.
    var min_size = adjustment + $('li', dialog).length * item_width;
    if (min_size < new_size) {

      new_size = min_size;
    }

    // Some basic sanity checking to make sure we don't size too small or large.
    if (new_size > item_width && new_size < $(window).width()) {

      dialog.dialog('option', 'width', new_size + 'px');

      // Reposition because the size has changed.
      dialog.dialog('option', 'position', 'center');
    }
  }

  /**
   * Displays the album browse dialog and binds events.
   */
  function show_browse_dialog() {

    var dialog = create_browse_dialog();

    // Handle clicking on an album's cover photo or name.
    dialog.on('click', '.fb-album', function (e) {

      var album = facebook_albums.getByID($(this).attr('id'));
      if (false == album) {

        alert('Unable to locate selected album.');
        dialog.dialog('close');
        return false;
      }

      dialog.html(album.renderPhotos());
      resize_browse_dialog(dialog);
    });

    // Handle clicking on a specific album image.
    dialog.on('click', '.fb-album-images .fb-album-image', function (e) {

      var parent = $(this).parents('.fb-album-images');
      var album = facebook_albums.getByID(parent.attr('id'));

      // Swap selected class to the current image.
      $('.selected', parent).removeClass('selected');
      $(this).parent().addClass('selected');

      var photo = album.getPhotoByID($('img', this).attr('id'));
      if (false == photo) {

        alert('Unable to get selected photo.');
        dialog.dialog('close');
        return false;
      }

      var input = $('<input></input>')
        .attr('type', 'hidden')
        .attr('name', 'fb-album-photo')
        .attr('value', JSON.stringify({
            id: photo.id,
            token: FB.getAccessToken()
          }));

      var form = $('#dfil-happimess-share-form');
      $('input[name="fb-album-photo"]', form).remove();
      form.append( input );

      $('input[name="allow_text_only"]').val('0');

      show_selected_fb_image(photo, album);

      // Add class so we can style button to indicate a photo was selected.
      $('#edit-fb-button', form).addClass('photo-selected');
      dialog.dialog('close');
    });

    dialog.append(Drupal.theme('facebookAlbums', facebook_albums));
    resize_browse_dialog(dialog);
    dialog.dialog('open');
  };

  /**
   * Replace the file upload selection area with a FB image preview.
   *
   * @param object photo
   *   The selected photo.
   *
   * @param object album
   *   The album the selected photo belongs to.
   */
  function show_selected_fb_image(photo, album) {

    // Hide the photo and video tabs + content.
    $('#edit-options-wrap, #edit-plus-photo, #edit-plus-video')
      .addClass('fb-preview-hide')
      .hide();

    // Set up an event handler for when the user cancels the form.
    $('#edit-cancel')
      .once()
      .on('click', function (e) {

        // Remove our image preview.
        $('.fb-preview').remove();

        // Remove the selected photo information.
        $('input[name="fb-album-photo"]').remove();
        $('#edit-fb-button').removeClass('photo-selected');

        // Allow creating text only posts.
        $('input[name="allow_text_only"]').val('1');

        // Restore the hidden tabs and content.
        $('.fb-preview-hide')
          .removeClass('fb-preview-hide')
          .show();
      });

    // Add the Facebook selection preview before the submit button.
    $('#edit-submit')
      .before(Drupal.theme('facebookSelectedPhotoPreview', photo, album));
  }

  Drupal.behaviors.happimess_fb_import = {

    attach: function (context, settings) {

      $('#dfil-happimess-share-form', context)
        .once()
        .on('click', '.dfil-fb-upload', function (e) {

          e.preventDefault();

          // Only attempt to run FB code if the SDK has loaded.
          if (!$('body').hasClass('fb-js-sdk')) {

            return;
          }

          // If we have already retrieved the user's albums then just show the
          // album browser and exit.
          if (facebook_albums.length > 0) {

            show_browse_dialog();
            return;
          }

          // Make a request to the FB servers for this users album
          // information. If they have not given us permission this will
          // automatically handle prompting them to give us access.
          FB.login(function() {
            FB.api('/me/albums', 'get', {
              // By default we do not get the album photos in one request.
              // Adding these fields here allows us to make a single request
              // to get all the information we need instead of multiple API
              // calls.
              'fields': 'id,name,cover_photo,photos.fields(id,picture,source,name)'
            }, function (response) {

              if (!response || response.error) {

                alert('Unable to fetch Facebook albums.');
                return;
              }

              process_fb_albums(response.data);
              if (facebook_albums.length > 0) {

                show_browse_dialog();
              }
              else {

                alert(Drupal.t('You do not seem to have any albums.'));
              }
            });
          }, {scope: 'user_photos'});

        });
    }
  };
})(jQuery, Drupal);
