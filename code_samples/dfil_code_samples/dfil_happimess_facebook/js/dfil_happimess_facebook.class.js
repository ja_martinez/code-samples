/**
 * @file
 * Classes to make working with albums easier.
 */
"use strict";

/**
 * Create an extension of Array() to make working with an array of albums easier.
 */
function FacebookAlbumCollection() {}
FacebookAlbumCollection.prototype = new Array();

/**
 * Helper method so that we can easily clear the array.
 *
 * We can't simply do collection_items = [] because that will replace our
 * collection object with an array. We could do collection_items.length = 0, but
 * it reads better to have a method call.
 */
FacebookAlbumCollection.prototype.clear = function () {

  this.length = 0;
};

/**
 * Gets an album from the collection that has the given ID.
 * @param string seek_id
 *   The album ID to look up.
 *
 * @return FacebookAlbum|false
 *   Returns a FacebookAlbum object or false if no album was found.
 */
FacebookAlbumCollection.prototype.getByID = function (seek_id) {

  var matching = this.filter(function (element) {

    return element.info.id == seek_id;
  });

  return matching.length > 0 ?
    matching[0] :
    false;
};

/**
 * Wraps the album items returned by the Facebook API.
 */
function FacebookAlbum(info) {

  this.info = info;
};

/**
 * Get the album's cover photo if available.
 *
 * @return object|false
 *   The cover photo or the first available photo if the cover photo
 *   could not be found. If the album has no photos then result is false.
 */
FacebookAlbum.prototype.getCoverPhoto = function () {

  if (0 == this.info.photos.data.length) {

    return false;
  }

  var first_photo = this.info.photos.data[0];

  for (var i = 0;i < this.info.photos.data.length; i++) {

    var photo = this.info.photos.data[i];
    if (photo.id == this.info.cover_photo) {

      return photo;
    }
  }

  return first_photo;
};

/**
 * Renders this album.
 *
 * @return string
 *   The rendered album.
 */
FacebookAlbum.prototype.render = function () {

  return Drupal.theme('facebookAlbum', this);
};

/**
 * Renders this album's photos.
 *
 * @return string
 *   This album's rendered photos.
 */
FacebookAlbum.prototype.renderPhotos = function () {

  return Drupal.theme('facebookAlbumImages', this);
}

/**
 * Gets the information for the photo with the given ID.
 *
 * @param string photo_id
 *   The photo ID.
 *
 * @return object|false
 *   The photo information or false if not found.
 */
FacebookAlbum.prototype.getPhotoByID = function (photo_id) {

  var matching = this.info.photos.data.filter(function (element) {

    return element.id == photo_id;
  });

  return matching.length > 0 ?
    matching[0] :
    false;
}
